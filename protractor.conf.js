exports.config = {
    logLevel:'INFO',
    specs: ['./e2e/features/*.feature'],
    capabilities: {
        'browserName': 'chrome'
    },
    directConnect: true,
    baseUrl: 'http://localhost:4200/',
    restartBrowserBetweenTests: false,
    SELENIUM_PROMISE_MANAGER: false,
    ignoreUncaughtExceptions: true,
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    cucumberOpts: {
        strict: true,
        //tags: ["@defect"],
        require: [
            './e2e/steps/*StepDef.js', './e2e/utility/hooks.js'
        ],
        format: 'json:reports/results.json'
    },
     onPrepare: function () {
    
        browser.waitForAngularEnabled();
        browser.driver.get("http://localhost:4200/");
        return browser.driver.wait(function () {
            return browser.driver.getCurrentUrl().then(function (url) {
                return url;
            });
        }, 10000); 
        
    }, 
    onComplete: function() {
        browser.driver.quit();
    },
    plugins: [
        {
            package: require.resolve('protractor-multiple-cucumber-html-reporter-plugin'),
            options: {
                automaticallyGenerateReport: true,
                removeExistingJsonReportFile: true,
                removeExistingJsonReportFile: true,
                pageTitle: 'The Star Wars Search Application',
                saveCollectedJSON: true,
                jsonOutputPath: './reports/cucumber_html_report',
                reportName: 'The Star Wars Search Application Test Excecution Report',
                customMetadata: false,
                displayDuration: true,
                durationInMS: true,
                customData: {
                    title: 'Run info',
                    data: [
                        {
                            label: 'Project',
                            value: 'The Star Wars Search Application'
                        }, {
                            label: 'Release',
                            value: '1.2.3'
                        }, {
                            label: 'Cycle',
                            value: 'B11221.34321'
                        },{
                            label: 'Created by',
                            value: 'Onkar Bhagwat'
                        }
                    ]
                }


            }
        }
    ],
    params: {
        webDriverWaitTime:5000
      }
};

#Please refer README.md file as below sequence of Scenario followes the same.
Feature: Verify search functionality of The Star Wars Search Application when People radio button selected

    #Test case 1: When you search for **a character** and it’s a valid one, then you should be able to see his / her “Gender”, “Birth year”, “Eye color” and “Skin color”.
    Scenario Outline: Verify search functionality for valid Characters(person) when People radio button selected
        Given User is navigated to The star Wars Search Application
        When User search for "<Character>" when People radio button selected
        Then User should be able to see his or her "<Gender>","<Birth year>","<Eye color>","<Skin color>"

        Examples:
            | Character      | Gender | Birth year | Eye color | Skin color  |
            | Luke Skywalker | male   | 19BBY      | blue      | fair        |
            | Leia Organa    | female | 19BBY      | brown     | light       |
            | R2-D2          | n/a    | 33BBY      | red       | white, blue |
            | Darth Vader    | male   | 41.9BBY    | yellow    | white       |


    #Test case2: When you search for a character and it’s not a valid one, then you should be able to see “Not found” in the results.
    Scenario Outline: Verify search functionality for invalid Characters(person) when People radio button selected
        Given User is navigated to The star Wars Search Application
        When User search for "<InvalidCharacter>" when People radio button selected
        Then User should be shown Not found as a result message

        Examples:
            | InvalidCharacter |
            | xyz              |
            | AABB             |
            | AAbb             |
            | A@b              |
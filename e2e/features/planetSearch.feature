#Please refer README.md file as below sequence of Scenario followes the same.
Feature: Verify search functionality of The Star Wars Search Application when Planet radio button selected

    #Test case 1:When you search for **a planet** and it’s a valid one, then you should be able to see its “Population”, “Climate” and “Gravity”.
    Scenario Outline: Verify search functionality for valid Planets when Planet radio button selected
        Given User is navigated to The star Wars Search Application
        When User search for "<Planet>" when Planets radio button selected
        Then User should be able to see "<Population>","<Climate>","<Gravity>"

        Examples:
            | Planet   | Population | Climate   | Gravity      |
            | Alderaan | 2000000000 | temperate | 1 standard   |
            | Hoth     | unknown    | frozen    | 1.1 standard |

    #Test case 2: When you search for a planet and it’s not a valid one, then you should be able to see “Not found” in the results.
    Scenario Outline: Verify search functionality for valid Planets when People radio button selected
        Given User is navigated to The star Wars Search Application
        When User search for "<InvalidPlanet>" when Planets radio button selected
        Then User should be shown Not found as a result message

        Examples:
            | InvalidPlanet |
            | xyz           |
            | AABB          |
            | AAbb          |
            | A@b           |
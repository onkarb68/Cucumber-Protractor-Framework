#Please refer README.md file as below sequence of Scenario followes the same.
Feature: Verify addional other flows of The Star Wars Search Application

    #Test case 1 for People:When you search for either a character or a planet and you get one or more results for it, clear the “Search form” and hit the Search button again, you should then get an empty result list (previous search results are removed).
    # Defect: Its defect
    # Ideally Below asertion should fail as per feture expecations i.e. Previous serach results should get removed, but its not working, Previous serach results are not getting removed. It should be reported as Defect.
    @defect
    Scenario: [Defect]Verify various negative use cases of The Star Wars Search Application for character(people)
        Given User is navigated to The star Wars Search Application
        When User search for people "Luke Skywalker"
        Then User gets one or more results for people
         #Covered one of the requirment to click by Keyboard control Enter key press
        And User clears the search form & searches again
        Then Previous serach results are not getting removed

    #Test case 2 for planet:When you search for either a character or a planet and you get one or more results for it, clear the “Search form” and hit the Search button again, you should then get an empty result list (previous search results are removed).
    # Defect: Its defect
    # Ideally Below asertion should fail as per feture expecations i.e. Previous serach results should get removed, but its not working, Previous serach results are not getting removed. It should be reported as Defect.
    @defect
    Scenario: [Defect]Verify various negative use cases of The Star Wars Search Application for character(people)
        Given User is navigated to The star Wars Search Application
        When User search for planet "Alderaan"
        Then User gets one or more results for planet
        #Covered one of the requirment to click by Keyboard control Enter key press
        And User clears the search form & searches again
        Then Previous serach results are not getting removed


    #Test case 3:When for example you have searched for a full planet name and you’ve got results, if you switch to People and search for the same thing (that has no matching people based on a partial name), you should get a “Not found” in the results.
    Scenario: Verify user shown Not found as a result message when views are switched when absence appropriate data
        Given User is navigated to The star Wars Search Application
        When User search for planet "Alderaan"
        Then User gets one or more results for planet
        And User switches to People & press Enter button for searching
        Then User should be shown Not found as a result message

    #Test case 4: You can have more than one results, for both Planets and Names (partial matching)
    Scenario: Verify user shown multiple search results inline with partials matching of search query
        Given User is navigated to The star Wars Search Application
        When User search for people "ar"
        Then Search results displayed as per matching partial input search query
        When User search for planet "al"
        Then Search results displayed as per matching partial input search query
const {When, Then} = require('cucumber');
const chai = require('chai');
chai.use(require('chai-string'));
const expect = chai.expect

var homepage = require('../pageObjects/homePage');
var searchpage = require('../pageObjects/searchPage');
var testDataPeople = require('../testData/charactersData.json')
var testDataPlant = require('../testData/planetsData.json')
const uiActionsObj = require('../utility/uiActions');
let searchResult;

When('User search for people {string}', async (name) => {

    await uiActionsObj.clickOnElement(homepage.radioButtonPeople);
    searchResult = name;
    await homepage.searchByPeople(searchResult);

});

Then('User gets one or more results for people', async () => {

    expect(await uiActionsObj.getElementText(searchpage.displayName)).to.equal(searchResult);

    var listOfPersonAttributes = await searchpage.getPersonPlanetAttributes();
    expect(await listOfPersonAttributes[0].getText()).to.equal(testDataPeople.characters[0].genderCharacteristics);
    expect(await listOfPersonAttributes[1].getText()).to.equal(testDataPeople.characters[0].birthYearCharacteristics);
    expect(await listOfPersonAttributes[2].getText()).to.equal(testDataPeople.characters[0].eyeColourfirstCharacteristics);
    expect(await listOfPersonAttributes[3].getText()).to.equal(testDataPeople.characters[0].skinColourCharacteristics);

});

Then('User clears the search form & searches again', async () => {

    await uiActionsObj.clearText(homepage.searchBox);

    //Covered one of the requirment to click by Keyboard control Enter key press
    await uiActionsObj.pressEnterButton();

});

Then('Previous serach results are not getting removed', async () => {

    // Defect: Its defect
    // Ideally Below asertion should fail as per feture expecations i.e. Previous serach results should get removed, but its not working, Previous serach results are not getting removed. It should be reported as Defect.
    expect(await homepage.isRadioButtonPeopleDisplayed()).to.equal(false);

});


When('User search for planet {string}', async (name) => {

    await uiActionsObj.clickOnElement(homepage.radioButtonPlanets);
    searchResult = name;
    await homepage.searchByPeople(searchResult);

});

Then('User gets one or more results for planet', async () => {

    var listOfPlanetAttributs = await searchpage.getPersonPlanetAttributes();

    expect(await uiActionsObj.getElementText(searchpage.displayName)).to.equal(searchResult);

    expect(await listOfPlanetAttributs[0].getText()).to.equal(testDataPlant.planets[0].populationAttribute);
    expect(await listOfPlanetAttributs[1].getText()).to.equal(testDataPlant.planets[0].climateAttribute);
    expect(await listOfPlanetAttributs[2].getText()).to.equal(testDataPlant.planets[0].gravityAttribute);

});

Then('User switches to People & press Enter button for searching', async () => {

    await uiActionsObj.clickOnElement(homepage.radioButtonPeople);
    await uiActionsObj.pressEnterButton();
    
});

Then('Search results displayed as per matching partial input search query', async () => {

    var listOfPeoplePlanetsName = await searchpage.getPeoplePlanetsName();

    for (var i = 0; i < listOfPeoplePlanetsName.length; i++) {
        let name = await listOfPeoplePlanetsName[i].getText();

        expect(await name).to.containIgnoreCase(searchResult);
    }
});

const {When, Then} = require('cucumber');
const chai = require('chai');
const expect = chai.expect

var homepage = require('../pageObjects/homePage');
var searchpage = require('../pageObjects/searchPage');
var testData = require('../testData/planetsData.json')
const uiActionsObj = require('../utility/uiActions');
let planetName;

When('User search for {string} when Planets radio button selected', async (planetNameVal) => {

    await uiActionsObj.clickOnElement(homepage.radioButtonPlanets);
    planetName = planetNameVal;
    await homepage.searchByPeople(planetName);

});

// #Test case 1:When you search for **a planet** and it’s a valid one, then you should be able to see its “Population”, “Climate” and “Gravity”.
Then('User should be able to see {string},{string},{string}', async (population, climate, gravity) => {

    var listOfPlanetAttributs = await searchpage.getPersonPlanetAttributes();
    var listOfPlanetAttributesValue = await searchpage.getPersonPlanetAttributesValues();

    // Validation of planet name got searched
    expect(await uiActionsObj.getElementText(searchpage.displayName)).to.equal(planetName);

    // Validation of “Population”, “Climate” and “Gravity”.
    expect(await listOfPlanetAttributs[0].getText()).to.equal(testData.planets[0].populationAttribute);
    expect(await listOfPlanetAttributs[1].getText()).to.equal(testData.planets[0].climateAttribute);
    expect(await listOfPlanetAttributs[2].getText()).to.equal(testData.planets[0].gravityAttribute);

    // Validation of values of “Population”, “Climate” and “Gravity”.
    expect(await listOfPlanetAttributesValue[0].getText()).to.equal(population);
    expect(await listOfPlanetAttributesValue[1].getText()).to.equal(climate);
    expect(await listOfPlanetAttributesValue[2].getText()).to.equal(gravity);

});

const {Given, When, Then} = require('cucumber');
const chai = require('chai');
const expect = chai.expect

var homepage = require('../pageObjects/homePage');
var searchpage = require('../pageObjects/searchPage');
var testData = require('../testData/charactersData.json')
const uiActionsObj = require('../utility/uiActions');
let characterName;

Given('User is navigated to The star Wars Search Application', async () => {

    //Validation for correct landing page, application URL
    expect(await homepage.isRadioButtonPeopleDisplayed()).to.equal(true);
    expect(await homepage.isRadioButtonPlanetDisplayed()).to.equal(true);
    expect(await homepage.isSearchBoxDisplayed()).to.equal(true);
    expect(await uiActionsObj.getCurrentUrl()).to.contains('http://localhost:4200/');

});

When('User search for {string} when People radio button selected', async (personName) => {

    await uiActionsObj.clickOnElement(homepage.radioButtonPeople);
    characterName = personName;
    await homepage.searchByPeople(personName);

});

// Test case 1: When you search for **a character** and it’s a valid one, then you should be able to see his / her “Gender”, “Birth year”, “Eye color” and “Skin color”.
Then('User should be able to see his or her {string},{string},{string},{string}', async (gender, birthYear, eyeColor, skinColor) => {

    var listOfPersonAttributes = await searchpage.getPersonPlanetAttributes();
    var listOfPersonAttributesValue = await searchpage.getPersonPlanetAttributesValues();

    // Validation of character name got searched
    expect(await uiActionsObj.getText(searchpage.displayName)).to.equal(characterName);

    // Validation of “Gender”, “Birth year”, “Eye color” and “Skin color”.
    expect(await uiActionsObj.getText(listOfPersonAttributes[0])).to.equal(testData.characters[0].genderCharacteristics);
    expect(await uiActionsObj.getText(listOfPersonAttributes[1])).to.equal(testData.characters[0].birthYearCharacteristics);
    expect(await uiActionsObj.getText(listOfPersonAttributes[2])).to.equal(testData.characters[0].eyeColourfirstCharacteristics);
    expect(await uiActionsObj.getText(listOfPersonAttributes[3])).to.equal(testData.characters[0].skinColourCharacteristics);

    // Validation of values of “Gender”, “Birth year”, “Eye color” and “Skin color”. for that particular searched person
    expect(await uiActionsObj.getText(listOfPersonAttributesValue[0])).to.equal(gender);
    expect(await uiActionsObj.getText(listOfPersonAttributesValue[1])).to.equal(birthYear);
    expect(await uiActionsObj.getText(listOfPersonAttributesValue[2])).to.equal(eyeColor);
    expect(await uiActionsObj.getText(listOfPersonAttributesValue[3])).to.equal(skinColor);

});

// Test case2: When you search for a character and it’s not a valid one, then you should be able to see “Not found” in the results.
Then('User should be shown Not found as a result message', async () => { 
    
    // Validation of “Not found” result message
    expect(await uiActionsObj.getElementText(searchpage.notFoundLabel)).to.equal(testData.characters[1].notFoundMessage);

});

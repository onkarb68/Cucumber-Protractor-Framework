const {browser, element, by} = require('protractor');
const {protractor} = require('protractor/built/ptor');
const urlencode = require('urlencode');

// This is to handle synchronization, which is conditional wait for given ExpectedConditions
var EC = browser.ExpectedConditions;

// Value webDriverWait is picked up from protractor.conf.js file which keeps it consistent across framework.
const webDriverWait = parseInt(`${
    urlencode(browser.params.webDriverWaitTime)
}`)

// This function returns webelement for the given ID locator
function getElement(locator) {

    let webElement = element(by.id(locator));
    browser.wait(EC.presenceOf(webElement), webDriverWait);
    browser.wait(EC.visibilityOf(webElement), webDriverWait);
    return element(by.id(locator));

}

// This function returns webelement for the given CSS locator
function getElementbyCss(locator) {

    let webElement = element(by.css(locator));
    browser.wait(EC.presenceOf(webElement), webDriverWait);
    browser.wait(EC.visibilityOf(webElement), webDriverWait);
    return element(by.css(locator));

}

// This function returns list of webelements for the given ID locator
function getElementsbyCss(locator) {

    element.all(by.css(locator));
    return element.all(by.css(locator));

}

// This function clicks on webelement for the given webElement
function clickOnElement(webElement) {

    return webElement.click();

}

// This function send the text on webelement for the given webElement
function enterText(webElement, text) {

    webElement.clear();
    return webElement.sendKeys(text);

}

// This function retunrs the current window url
function getCurrentUrl() {

    return browser.getCurrentUrl();

}

// This function gets the text of webelement for the given webElement
function getElementText(webElement) {

    let text = webElement.getText();
    return text;

}

// This function clears the inside text of webelement for the given webElement
function clearText(webElement) {

    return webElement.clear();

}

// This function performs the press of enter key from keyboard
function pressEnterButton() {

    return browser.actions().sendKeys(protractor.Key.ENTER).perform();

}

// This function gets the text of webelement for the given webElement
function getText(webElement) {

    let text = webElement.getWebElement().getText();
    return text;

}

// This function gets the title of current window
function getTitle() {

    let text = browser.getTitle();
    return text;

}

// This function returns the boolean for the absence of on webelement for the given webElement
function isElementNotDisplayed(locator) {

    return browser.wait(EC.invisibilityOf(element(by.css(locator))));

}

// This function returns the boolean for the display of on webelement for the given webElement
function isElementDisplayed(webElement) {

    let element = webElement.isDisplayed();
    return element;

}


exports.getElement = getElement;
exports.clickOnElement = clickOnElement;
exports.enterText = enterText;
exports.getCurrentUrl = getCurrentUrl;
exports.getElementsbyCss = getElementsbyCss;
exports.getElementText = getElementText;
exports.clearText = clearText;
exports.getElementbyCss = getElementbyCss;
exports.isElementNotDisplayed = isElementNotDisplayed;
exports.pressEnterButton = pressEnterButton;
exports.getText = getText;
exports.getTitle = getTitle;
exports.isElementDisplayed = isElementDisplayed;

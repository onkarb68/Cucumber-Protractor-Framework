var {
    Status,
    defineSupportCode
} = require('cucumber');
const {browser} = require('protractor');

defineSupportCode(({Before, After}) => {
    Before(function (scenario) {
        //Maximize browser window
        browser.driver.manage().window().maximize();
    });

    After(function (scenario) {
        browser.driver.executeScript('window.sessionStorage.clear();');
        browser.driver.executeScript('window.localStorage.clear();');
        browser.driver.manage().deleteAllCookies();
        let afterHook;
        //To take failed test screenshot
        if (scenario.result.status === Status.FAILED) {
            afterHook = browser.driver.takeScreenshot().then((screenshot) => {
                const self = this;
                self.attach(Buffer.from(screenshot.replace(/^data:image\/png;base64,/, ''), 'base64'), '"image/png"')
            })

        }
        return afterHook;
    });
});

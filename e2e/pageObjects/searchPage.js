const uiActionsObj = require('../utility/uiActions');

var displayNameLocator = 'app-root h6';

var displayName = uiActionsObj.getElementbyCss(displayNameLocator);
var notFoundLabel = uiActionsObj.getElementbyCss('div:nth-child(5)');

//This function returns the list of all Person or Planet Attributes like "<Gender>","<Birth year>","<Eye color>","<Skin color>","<Population>","<Climate>","<Gravity>"
async function getPersonPlanetAttributes() {
    var list = uiActionsObj.getElementsbyCss('.col-sm-2');
    return list;
};

//This function returns the list of all Person or Planet Attributes values as listed above
async function getPersonPlanetAttributesValues() {
    var list = uiActionsObj.getElementsbyCss('.col-sm-10');
    return list;
};

//This function returns the list of all Person or Planet names present on page
async function getPeoplePlanetsName() {
    var list = uiActionsObj.getElementsbyCss(displayNameLocator);
    return list;
};

//This function returns boolean to find display name
async function isDisplayNameDisplayed() {
    return await uiActionsObj.isElementDisplayed(displayName);
};

exports.displayName = displayName;
exports.notFoundLabel = notFoundLabel;
exports.getPersonPlanetAttributes = getPersonPlanetAttributes;
exports.getPersonPlanetAttributesValues = getPersonPlanetAttributesValues;
exports.getPeoplePlanetsName = getPeoplePlanetsName;
exports.isDisplayNameDisplayed = isDisplayNameDisplayed;


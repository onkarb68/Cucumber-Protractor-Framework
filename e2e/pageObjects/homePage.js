const uiActionsObj = require('../utility/uiActions');

var radioButtonPeople =  uiActionsObj.getElement('people');
var radioButtonPlanets =  uiActionsObj.getElement('planets');
var searchBox =  uiActionsObj.getElement('query');
var searchButton =  uiActionsObj.getElementsbyCss('app-search-form div:nth-child(4) > button');

//This function returns boolean to find display of Radio Button People
async function isRadioButtonPeopleDisplayed() { 
    return await uiActionsObj.isElementDisplayed(radioButtonPeople)
};

//This function returns boolean to find display of Radio Button Planet
async function isRadioButtonPlanetDisplayed() { 
    return await uiActionsObj.isElementDisplayed(radioButtonPlanets)
};

//This function returns boolean to find display of Search Box
async function isSearchBoxDisplayed() { 
    return await uiActionsObj.isElementDisplayed(searchBox)
};

//This function search for given people/character
async function searchByPeople(characterName) { 
    await uiActionsObj.clearText(searchBox);
    await uiActionsObj.enterText(searchBox, characterName);
    return await uiActionsObj.clickOnElement(searchButton);
}; 

exports.searchByPeople = searchByPeople;
exports.isRadioButtonPeopleDisplayed = isRadioButtonPeopleDisplayed;
exports.isRadioButtonPlanetDisplayed = isRadioButtonPlanetDisplayed;
exports.isSearchBoxDisplayed = isSearchBoxDisplayed;
exports.radioButtonPeople = radioButtonPeople;
exports.radioButtonPlanets = radioButtonPlanets;
exports.searchBox = searchBox;



##	Introduction
	This project demonstrates the QA UI test automation of The Star Wars Search Application using protractor-cucumber-framework

##	Contributor:
	Name: Onkar Bhagwat 
	Contact information: Email Id: onkarb68@gmail.com    Mb no: +918390436520

##	Language:
	Have used javascript as programming language. camelCase used as coding practice for better readability.

##	Cucumber feature file:
	Location: \e2e\features
	Have created 3 Cucumber feature files separately. 
	In additionalFlows.feature file, you will find “@defect” tag which is indication of 2 defects founds out in the application. Same 2 scenarios are showing failed in report as being defects as per below execution report , rest all are passing perfectly.
 
	Have made all testing assertions, validations in Then block, except its very essential(like to check landing page) in other Given or When block. As per official cucumber communication, Then steps should make assertions comparing expected results to actual results from your application.
	Have used Scenario outline for data driven testing. 

##	Utility:
	Location: \e2e\utility
	Utility package have 2 files. 
###	1.uiActions.js: 
	This file deals with all the common functions used by all the page Objects,  step defentions. This file have functions which are wrapper around core protractor(webdriver) commands. These functions would get consumed in all step definitions , page objects file. handles The whole intent of using this file is to have only one file to modify, upgrade, maintain framework seamlessly instead of altering hundreds of file if not done developed properly.
	Example:
	function getElement(locator) {
		let webElement = element(by.id(locator));
		browser.wait(EC.presenceOf(webElement), webDriverWait);
		browser.wait(EC.visibilityOf(webElement), webDriverWait);
		return element(by.id(locator));
	}
	We can see, this the basic function would be needed in all QA automation. Instead of writing same 6lines everywhere, better to call one function which implicitly handles synchronization using explicit wait.
### 2.Hooks.js: 
	This file handles before scenario and after scenario configuration like browser maximize and taking screenshot off failed test, delete cookies, session clear. 

##	Page Objects:
	Location: \e2e\pageObjects\
	These Page Object simply models these as objects within the test code. This reduces the amount of duplicated code and means that if the UI changes, the fix need only be applied in one place.
	The functions , elements designed in page objects file are using functions from uiActions.js for better maintainability of QA automation suite. 
	Example: var radioButtonPeople =  uiActionsObj.getElement('people');
	All the page objects are using either CSS or id to locate elements
	Have used Async/await to get stable e2e test without using control flow. As official protractor communication suggests to use of Async/await as control flow is being removed for better e2e stable execution.
	Example:
	async function searchByPeople(characterName) { 
		await uiActionsObj.clearText(searchBox);
		await uiActionsObj.enterText(searchBox, characterName);
		return await uiActionsObj.clickOnElement(searchButton);
	}; 

##	TestData
	Location: \e2e\testData\
	All the test test data been kept in .json file. We pass test data in step definition files for data-driven testing.

##	Step definition:
	Location: \e2e\steps\
	The glue code is responsible for orchestrating calls to a layer of more business-focused classes, which perform the actual REST calls to webdriver server.

##	Execution Report:
	Location: \e2e\reports\report\index.html
	For the reporting purpose, have used protractor-multiple-cucumber-html-reporter-plugin plugin. It generates beautiful HTML reports.

##	Running end-to-end tests
	Run command `npm run e2e` to execute the end-to-end tests via this framework
 
##	Changes made in existing configuration

###	protractor.conf.js
	Have added below protractor config parameters.
	1.	logLevel:'INFO',
	2.	restartBrowserBetweenTests: false,
	3.	SELENIUM_PROMISE_MANAGER: false,
	4.	ignoreUncaughtExceptions: true,
	5.	frameworkPath: require.resolve('protractor-cucumber-framework'),
	6.	tags: ["@defect"],
	7.	require: [
				'./e2e/steps/*StepDef.js', './e2e/utility/hooks.js'
			],
	8.	onPrepare: function () {
	9.	    
			browser.waitForAngularEnabled();
		   browser.driver.get("http://localhost:4200/");
		   return browser.driver.wait(function () {
			   return browser.driver.getCurrentUrl().then(function (url) {
				   return url;
				});
			}, 10000); 
		   
	   }, 
	10.	onComplete: function() {
		   browser.driver.quit();
		},
	Added reporting plugin package: require.resolve('protractor-multiple-cucumber-html-reporter-plugin'),

###	.gitignore file
	Have modified. gitignore file to ignore unnecessary files from reports folder

###	package.json
	Have installed few libraries like chai-string, protractor-cucumber-framework, protractor-multiple-cucumber-html-reporter-plugin, urlencode
	
###	.vscode/settings.json
	Have added .vscode/settings.json file for cucumber extensions to work seamlessly


